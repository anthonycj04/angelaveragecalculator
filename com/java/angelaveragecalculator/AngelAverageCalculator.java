package com.java.angelaveragecalculator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class AngelAverageCalculator {
	private class Info{
		int numOfFiles = 0;
		int numOfLines = 0;
		int counterStart = 0;
		int counterEnd = 0;
		String filename = null;
		String fileExtension = null;
		String type = null;
	}

	private Queue<Info> infoQueue;
	private String logFolder = "experiment_dynamic_1/LOG/";
	private String executeFunction = "sfg";
	public static void main(String[] args){
		(new AngelAverageCalculator()).start();
	}

	private void start(){
		if (executeFunction.equals("first")){
			infoQueue = new LinkedList<Info>();
			insertInfo(20, 0, 0, 0, "serving_gw1_", "txt", "variance");
			insertInfo(20, 0, 0, 0, "serving_gw2_", "txt", "variance");
			insertInfo(20, 0, 0, 0, "serving_gw3_", "txt", "variance");
			insertInfo(20, 998, 1, 9, "queue", "log", "average");
			while (infoQueue.peek() != null){
				Info tempInfo = infoQueue.poll();
				if (tempInfo.type == "variance")
					calculateVariance(tempInfo.numOfFiles, tempInfo.filename, tempInfo.fileExtension);
				else
					calculateAverage(tempInfo.numOfFiles, tempInfo.numOfLines, tempInfo.counterStart, tempInfo.counterEnd, tempInfo.filename, tempInfo.fileExtension);
			}
		}
		else{
			calculateSingleFileAverage("serving_gw2_variance_d30.txt");
			calculateSingleFileAverage("serving_gw2_variance_d31.txt");
			calculateSingleFileAverage("serving_gw2_variance_d32.txt");
			calculateSingleFileAverage("serving_gw3_variance_s31.txt");
		}
	}

	private void calculateSingleFileAverage(String filename){
		BufferedReader bufferedReader = null;
		ArrayList<Double> data = new ArrayList<Double>();
		ArrayList<Double> data2 = new ArrayList<Double>();
		try {
			bufferedReader = new BufferedReader(new FileReader(logFolder + filename));
			String line = null;
			String [] splittedLine = null;
			while ((line = bufferedReader.readLine()) != null){
				splittedLine = line.replace(' ', '\t').replaceAll("\t\t", "\t").split("\t");
				data.add(Double.valueOf(splittedLine[1]));
				data2.add(Double.valueOf(splittedLine[2]));
			}
			bufferedReader.close();
			System.out.println(getMean(data) + "\t" + getMean(data2));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void insertInfo(int numOfFiles, int numOfLines, int counterStart, int counterEnd, String filename, String fileExtension, String type){
		Info newInfo = new Info();
		newInfo.numOfFiles = numOfFiles;
		newInfo.numOfLines = numOfLines;
		newInfo.counterStart = counterStart;
		newInfo.counterEnd = counterEnd;
		newInfo.filename = logFolder + filename;
		newInfo.fileExtension = fileExtension;
		newInfo.type = type;
		infoQueue.offer(newInfo);
	}

	private void calculateVariance(int numOfFiles, String filename, String fileExtension){
		BufferedReader bufferedReader = null;
		PrintWriter printWriter = null;
		ArrayList<Double> data = new ArrayList<Double>();
		try {
			data.clear();
			printWriter = new PrintWriter(new FileOutputStream(filename + "variance." + fileExtension, false), true);
			String line = null;
			String [] splittedLine = null;
			for (int i = 1; i <= numOfFiles; i++){
				bufferedReader = new BufferedReader(new FileReader(filename + i + "." + fileExtension));
				while ((line = bufferedReader.readLine()) != null){
					splittedLine = line.replace(' ', '\t').replaceAll("\t\t", "\t").split("\t");
					data.add(Double.valueOf(splittedLine[1]));
				}
				bufferedReader.close();
				printWriter.println(i);
				printWriter.println("average: " + getMean(data));
				printWriter.println("variance: " + getVariance(data));
				printWriter.println();
			}
			printWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private double getMean(ArrayList<Double> data){
		double sum = 0.0;
		for (double a: data)
			sum += a;
		return sum / data.size();
    }

	double getVariance(ArrayList<Double> data){
		double mean = getMean(data);
		double temp = 0;
		for(double a: data)
			temp += (mean - a) * (mean - a);
		return temp / data.size();
	}

	private void calculateAverage(int numOfFiles, int numOfLines, int counterStart, int counterEnd, String filename, String fileExtension){
		BufferedReader bufferedReader = null;
		PrintWriter printWriter = null;
		String line = null;
		String[] splittedLine = null;
		float[][] averages = new float[numOfLines][counterEnd - counterStart + 1];
		for (int i = 0; i < numOfLines; i++){
			for (int j = 0; j < counterEnd - counterStart + 1; j++)
				averages[i][j] = 0;
		}
		try {
			for (int i = 1; i <= numOfFiles; i++){
				bufferedReader = new BufferedReader(new FileReader(filename + i + "." + fileExtension));
				for (int j = 0; j < numOfLines; j++){
					// iterate through lines
					line = bufferedReader.readLine();
					line = line.replace(' ', '\t');
					line = line.replaceAll("\t\t", "\t");
					splittedLine = line.split("\t");
					for (int k = counterStart; k <= counterEnd; k++){
						// iterate through counters
						averages[j][k - counterStart] += Double.valueOf(splittedLine[k]);
					}
				}
				bufferedReader.close();
			}
			printWriter = new PrintWriter(new FileOutputStream(filename + "averages.txt", false), true);
			for (int i = 0; i < numOfLines; i++){
				for (int j = 0; j < counterEnd - counterStart + 1; j++){
					printWriter.print(averages[i][j] / numOfFiles+ "\t");
				}
				printWriter.println();
			}
			printWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
